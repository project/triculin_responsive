Introduction
-----------

triculin responsive is a light weight theme with minimalistic and simple design approach
and second is content.

  FEATURES:-
  --------
  
   - Responsive, Mobile-Friendly Theme
   - Mobile support (Tablet, Android, iPhone, etc)
   - Minimal design and nice typography
   - Supported standard theme features: user picture in comment and posts etc

Requirements
------------

No special requirements

Installation
------------
* Clone theme into sites/all/themes from this link
git clone --branch 7.x-1.x http://git.drupal.org/sandbox/sumitkumar/2467223.git
himalaya
  Its create himalaya folder in sites/all/themes

* https://www.drupal.org/node/2470681 for further information.

* Enable the theme Aministration Menu.
  Aministration Menu >> Appearance 
  Chosse theme

Configuration
-------------

Find the configuration of theme here:
<strong>admin/appearance/settings/triculin_responsive </strong>
